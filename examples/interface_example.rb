class InterfaceExample
  interface java.lang.Cloneable if defined? Ruby2Java
  
  def self.main(args); end

  if defined? Ruby2Java
    static_signature :main, [java.lang.String[]] => Java::void
  end
end
