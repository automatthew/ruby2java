class InheritanceExample
  extends_class Java::OrgJruby.RubyFixnum if defined? Ruby2Java 
  
  def self.main(args); end
  
  if defined? Ruby2Java
    static_signature :main, [java.lang.String[]] => Java::void
  end
end
