# The examples really ought to work all the time.
require File.join(here = File.dirname(__FILE__), "..", "spec_helper")

examples = { 
  "InterfaceExample" => "interface_example.rb",
  # "InheritanceExample" => "inheritance_example.rb",
  "Metaprogramming" => "metaprogramming.rb"
  }
example_dir = "#{here}/../../examples"

examples.each do |classname, file|
  describe classname do
    before(:all) do
      Ruby2Java.compile(BuildDir, classname, "#{example_dir}/#{file}")
    end
    
    it "works from Ruby" do
      java_run(classname).should be_true
    end
    
    if File.exist?("#{example_dir}/#{classname}Test.java")
      it "works from Java" do
        java_compile("#{classname}Test.java").should be_true
        java_run("#{classname}Test").should be_true
      end
    end
    
  end
end

