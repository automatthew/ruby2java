require File.join(File.dirname(__FILE__), "..", "spec_helper")

describe "A compiled class's" do
  
  describe "static method" do
    before(:all) do
      Ruby2Java.compile(BuildDir, "StaticMethodArgs", "#{SourceDir}/static_method_args.rb")
    end
    
    it "receives correct arguments when called from Ruby" do
      java_run("StaticMethodArgs", "foo").should be_true
    end

    it "receives correct args when called from Java" do
      java_compile("StaticMethodArgsTest.java").should be_true
      java_run("StaticMethodArgsTest", "foo").should be_true
    end

  end
  
  describe "instance method" do
    before(:all) do
      Ruby2Java.compile(BuildDir, "MethodArgs", "#{SourceDir}/method_args.rb")
    end
    
    it "receives correct arguments when called from Ruby" do
      java_run("MethodArgs", "foo").should be_true
    end
  
    it "receives correct args when called from Java" do
      java_compile("MethodArgsTest.java").should be_true
      java_run("MethodArgsTest", "foo").should be_true
    end
  end
  
end

