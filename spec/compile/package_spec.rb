require File.join(File.dirname(__FILE__), "..", "spec_helper")

describe "Class compiled with package declaration" do
  before(:all) do
    Ruby2Java.compile(BuildDir, "Package", "#{SourceDir}/package.rb")
  end

  it "can be executed" do
    java_run("org.jruby.ruby2java.Package").should be_true
  end
  
  it "can be imported" do
    java_compile("PackageTest.java").should be_true
    java_run("PackageTest", "foo").should be_true
  end

end