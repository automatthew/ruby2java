require File.join(File.dirname(__FILE__), "..", "spec_helper")

describe "Compiled methods" do
  before(:all) do
    Ruby2Java.compile(BuildDir, "ReturnVoid", "#{SourceDir}/return_void.rb")
    Ruby2Java.compile(BuildDir, "ReturnPrimitive", "#{SourceDir}/return_primitive.rb")
    Ruby2Java.compile(BuildDir, "ReturnObject", "#{SourceDir}/return_object.rb")
  end

  it "can return void" do
    java_run("ReturnVoid").should be_true
  end
  
  it "can return a primitive" do
    java_run("ReturnPrimitive").should be_true
  end
  
  it "can return an object" do
    java_run("ReturnObject").should be_true
  end
  

end