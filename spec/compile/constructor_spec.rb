require File.join(File.dirname(__FILE__), "..", "spec_helper")

describe "A constructor method registered on :initialize" do
  before(:all) do
    Ruby2Java.compile(BuildDir, "Constructor", "#{SourceDir}/constructor.rb")
  end

  it "works from Ruby" do
    java_run("Constructor").should be_true
  end

  it "works from Java" do
    java_compile("ConstructorTest.java").should be_true
    java_run("ConstructorTest").should be_true
  end
  

end