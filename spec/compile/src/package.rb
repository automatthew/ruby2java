class Package
  package "org.jruby.ruby2java" if defined? Ruby2Java
  
  def foo(arg)
    raise "expected 'foo', got #{arg}" unless arg == "foo"
  end
  
  def self.main(args)
    
  end
  
  if defined? Ruby2Java
    signature :foo, [java.lang.String] => Java::void
    static_signature :main, [java.lang.String[]] => Java::void
  end
end