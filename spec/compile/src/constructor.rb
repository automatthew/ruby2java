class Constructor
  def initialize(value)
    raise "expected 'foo', got #{value}" unless value == "foo"
  end
  
  def self.main(args)
    self.new("foo")
  end
  
  if defined? Ruby2Java
    signature :initialize, [java.lang.String] => Java::void
    static_signature :main, [java.lang.String[]] => Java::void
  end
end