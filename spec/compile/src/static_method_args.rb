class StaticMethodArgs

  def self.foo(arg)
    raise "expected 'foo', got #{arg}" unless arg == "foo"
  end
  
  def self.main(args)
    foo(args.first)
  end
  
  if defined? Ruby2Java
    static_signature :foo, [java.lang.String] => Java::void
    static_signature :main, [java.lang.String[]] => Java::void
  end
end