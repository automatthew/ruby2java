class ReturnPrimitive

  def return_primitive
    42
  end
  
  def self.main(args)
    self.new.return_primitive
  end
  
  if defined? Ruby2Java
    signature :return_primitive, [] => Java::int
    static_signature :main, [java.lang.String[]] => Java::void
  end
end