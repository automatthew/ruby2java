class MethodArgs

  def foo(arg)
    raise "expected 'foo', got #{arg}" unless arg == "foo"
  end
  
  def self.main(args)
    x = self.new
    x.foo(args.first)
  end
  
  if defined? Ruby2Java
    signature :foo, [java.lang.String] => Java::void
    static_signature :main, [java.lang.String[]] => Java::void
  end
end