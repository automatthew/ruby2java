class ReturnVoid
  
  def return_void
    
  end

  def self.main(args)
    self.new.return_void
  end
  
  if defined? Ruby2Java
    signature :return_void, [] => Java::void
    static_signature :main, [java.lang.String[]] => Java::void
  end
end