class ReturnObject
  
  def return_object
    "foo"
  end
  
  def self.main(args)
    self.new.return_object
  end
  
  if defined? Ruby2Java
    signature :return_object, [] => java.lang.String
    static_signature :main, [java.lang.String[]] => Java::void
  end
end