require File.join(File.dirname(__FILE__), 'spec_helper')

describe "Compiler" do
  
  describe "#write_files" do
    
    before(:each) do
      @source_file = "#{SourceDir}/useless.rb"
      require @source_file
      @ruby_class = Useless
      @compiler = Ruby2Java::JavaCompiler.new(@source_file)
      @compiler.process_class("Useless", Useless, "Useless", @source_file)
    end
    
    it "defaults to PWD as base output directory" do
      @compiler.write_files
      class_file = "Useless.class"
      exists = File.exist?(class_file)
      exists.should be_true
      FileUtils.rm(class_file) if exists
    end
    
    it "accepts an argument for base directory" do
      @compiler.write_files("spec/tmp")
      class_file = "spec/tmp/Useless.class"
      exists = File.exist?(class_file)
      exists.should be_true
      FileUtils.rm(class_file) if exists
    end
    
  end
  
end

