require File.join(File.dirname(__FILE__), 'spec_helper')

describe "Class package support" do
 
  describe "#package" do
    
    before :each do
      @class = Class.new
    end
    
    it "should not take into account consecutive calls after the first one" do  
      @class.package "org.jruby.ruby2java"
      @class.package "org.jruby"
      @class.package_name.should == "org.jruby.ruby2java"
    end

    it "should fail when called without arguments" do
      lambda { @class.package }.should raise_error
    end
  end
  
end

