here = File.expand_path File.dirname(__FILE__)
require File.join(here, "..", "lib", "ruby2java")

# Method naming convention: methodABCD
# A: number of compulsory arguments.
# B: number of optional parameters.
# C: 1 if restargs, 0 otherwise
# D: 1 if block_given?, 0 otherwise.


#  TODO: These compilation methods would work better for specs if 
# they were expressed as Rake tasks.  That way, we would
# not have to rebuild class files if the sources had not changed.
JRubyJar = "#{ENV_JAVA['jruby.lib']}/jruby.jar" unless defined?(JRubyJar)
SourceDir = "#{here}/compile/src" unless defined?(SourceDir)
BuildDir = "#{here}/compile/build" unless defined?(BuildDir)

$CLASSPATH << BuildDir unless $CLASSPATH.include?(BuildDir)

def java_compile(file_name)
  command = "javac -cp #{BuildDir}:#{JRubyJar} #{SourceDir}/#{file_name} -d #{BuildDir}"
  system(command)
end

def java_run(class_name, *args)
  command = "java -cp #{BuildDir}:#{JRubyJar} #{class_name} #{args.join(' ')}"
  system(command)
end

