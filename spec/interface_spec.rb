require File.join(File.dirname(__FILE__), 'spec_helper')

JCloneable = java.lang.Cloneable
JComparable = java.lang.Comparable

describe "Class interface support" do
  
  before :each do
    @class = Class.new
  end

  it "should be empty by default" do
    @class.interfaces.should be_empty
  end

  it "should retain interfaces added previously" do
     @class.interface JCloneable
     @class.interface JComparable
     @class.interfaces.should have(2).interfaces
  end

  it "should not add an already added interface" do
     @class.interface JCloneable
     @class.interface JCloneable
     @class.interfaces.should have(1).interfaces
  end

  it "should raise an exception if no arguments are passed" do
    lambda{
       @class.interface
    }.should raise_error(ArgumentError)
  end
end
