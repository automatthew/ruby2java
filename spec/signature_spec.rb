require File.join( File.dirname(__FILE__), "spec_helper")

describe "Class.signature" do
  before(:each) do
    @class = Class.new do
      
      def method0000
        puts "foo"
      end
      
      def method1100(a,b="bar")
        a + b
      end
      
    end
  end
  
  it "takes either symbol or string as the method name" do
    @class.signature :method0000, [] => Java::void
    @class.signature "method1100", [java.lang.String] => java.lang.String
    
    @class.signatures["method0000"].should have(1).signature
    @class.signatures["method1100"].should have(1).signature
  end
  
  it "retains only the last signature registered per method" do
    @class.signature :method0000, [] => Java::void
    @class.signature :method0000, [] => Java::int
    sigs = @class.signatures["method0000"]
    sigs.should have(1).signature
    sigs.first.retval.should == Java::int
  end
  
  describe "raises an exception if" do

    it "more arguments than the method can accept are given" do
      lambda{
        @class.signature "method0000", [java.lang.String] => Java::void
      }.should raise_error(ArgumentError)
    end
    
    it "less arguments than needed are given" do
      lambda{
        @class.signature "method1100", [] => Java::void
      }.should raise_error(ArgumentError)
    end

  end

  
end
