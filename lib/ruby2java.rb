require File.join(File.dirname(__FILE__), 'compiler')
require File.join(File.dirname(__FILE__), 'compilable')

module Ruby2Java
  VERSION='0.0.3'
  
  def self.compile(output_dir, ruby_class_name, *embed_files)
    embed_files.each { |file| require file }

    ruby_class = eval(ruby_class_name)

    java_compiler = Ruby2Java::JavaCompiler.new(embed_files[0])
    java_compiler.process_class(ruby_class_name, ruby_class, ruby_class_name, *embed_files)

    java_compiler.write_files(output_dir)
  end
  
end
